/*
   Old mouse wheel encoder as Jog Shuttle
*/

#ifndef Bounce2_h
  #include <Bounce2.h>
#endif

#define pinEncoderA 2
#define pinEncoderB 3

#define NUM_BUTTONS 3
#define RBP A5
#define MBP A3
#define LBP A4
#define RB 0
#define MB 1
#define LB 2

const uint8_t BUTTON_PINS[NUM_BUTTONS] = {RBP, MBP, LBP};
Bounce * buttons = new Bounce[NUM_BUTTONS];

//volatile boolean mov, begin_position;
volatile int position;
int lastPosition;
uint8_t i;
uint8_t buf[8] = { 0 }; /* Keyboard report buffer */
//======================================================================
void setup() {
  // инициализируем кнопки с антидребезгом
  for (i = 0; i < NUM_BUTTONS; i++) {
    buttons[i].attach(BUTTON_PINS[i], INPUT_PULLUP);
    buttons[i].interval(25); // задержка антидребезга
  }

  pinMode(pinEncoderA, INPUT_PULLUP);
  pinMode(pinEncoderB, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(pinEncoderA), encoderRotate, CHANGE);
  attachInterrupt(digitalPinToInterrupt(pinEncoderB), encoderRotate, CHANGE);

  Serial.begin(9600);
  position = 0;
  lastPosition = 0;
}
//======================================================================
void loop() {
  // проверим состояния всех кнопок
  for (i = 0; i < NUM_BUTTONS; i++) buttons[i].update();
  // если нажата красная кнопка, то у выходим с флагом изменения состояния
  if (buttons[RB].fell()) {
    buf[2] = 77;
    Serial.write(buf, 8);
    releaseKey();
    //Serial.println("R");
  }
  if (buttons[MB].fell()) {
    buf[2] = 85;
    Serial.write(buf, 8);
    releaseKey();
//    Serial.println("M");
  }
  if (buttons[LB].fell()) {
    buf[2] = 74;
    Serial.write(buf, 8);
    releaseKey();
//    Serial.println("L");
  }

  if (position != lastPosition) {
    Serial.println(position);
    lastPosition = position;
  }
}
//======================================================================
void releaseKey() {
  buf[0] = 0;
  buf[2] = 0;
  Serial.write(buf, 8); // Release key
}
//======================================================================
void encoderRotate() {
  static boolean mov, begin_position;

  boolean encA = digitalRead(pinEncoderA);
  boolean encB = digitalRead(pinEncoderB);

  if (encA == encB) {
    // А и Б = 0 или А и Б = 1
    mov = true;
    begin_position = encA;
  }  else {
    // А=0, Б=1 или А=1, Б=0
    if (mov) {
      if (begin_position) {
        //===
        if (!encA && encB) {
          // А=0 и Б=1
          mov = false;
          position++;
        }
        if (encA && !encB) {
          // А=1 и Б=0
          mov = false;
          position--;
        }
        //===
      } else {
        //===
        if (!encA && encB) {
          // А=0 и Б=1
          mov = false;
          position--;
        }
        if (encA && !encB) {
          // А=1 и Б=0
          mov = false;
          position++;
        }
        //===
      }
    }
  }

}
//======================================================================
